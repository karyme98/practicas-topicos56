package practicas;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author JOHANNA
 */
public class Practicas extends JFrame implements ActionListener{
    JLabel etiq1;
    JTextField nombre;
    JButton btn;
    
    Practicas(){
        this.setTitle("Practica1");
        this.setSize(300,150);
        this.setLayout(new FlowLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        etiq1 = new JLabel("escriba un nombre para saludar");
        nombre = new JTextField(20);
        btn = new JButton("Saludar");
        btn.addActionListener(this);
        
        this.add(etiq1);
        this.add(nombre);
        this.add(btn);
    }

    public static void main(String[] args) {
       java.awt.EventQueue.invokeLater(new Runnable(){
        public void run(){
            new Practicas().setVisible(true);
        }  
});
}

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this,"HOLA "+this.nombre.getText());
    }
    }