package Proyecto;

import java.io.*;
import java.net.Socket;
import java.util.Vector;
import javax.swing.DefaultListModel;


public class ChatServer extends Thread {
   private DataInputStream entrada;
    private DataOutputStream salida;
    private PanelServer server;
    private Socket Cliente;
    public static Vector<ChatServer> usuarioActivo = new Vector();//Almacen de atributos tipo hilo
    private String nombre;
    private ObjectOutputStream salidaObjeto;
    
    public ChatServer (Socket socketcliente, String nombre, PanelServer serv) throws Exception {
        this.Cliente = socketcliente;
        this.server = serv;
        this.nombre = nombre;
        usuarioActivo.add(this);
        
        for (int i = 0 ; i < usuarioActivo.size(); i++) {
            usuarioActivo.get(i).enviosMensajes(nombre + " Se ha conectado.");//Muestra usuarios activos y los que se conectan
        }
    }
    
    public void run() {
        String mensaje = " ";
        while (true){
            try {
                entrada = new DataInputStream(Cliente.getInputStream());
                mensaje = entrada.readUTF();
                
                for (int i = 0; i < usuarioActivo.size(); i++) {
                    usuarioActivo.get(i).enviosMensajes(mensaje);
                    server.mensajeria("Mensaje enviado");//Da la confirmacion de que ha sido enviado un mensaje de la parte Cliente
                }
                
            } catch (Exception e) {
                break;
            }
        }
        
        usuarioActivo.removeElement(this);
        server.mensajeria("El usuario se ha desconectado");//Informa cuando un cliente abandona el chat
        
        try{
            Cliente.close();
        } catch (Exception e){
            
        }
    }
    
    private void enviosMensajes(String msg) throws Exception{
        salida = new DataOutputStream(Cliente.getOutputStream());
        salida.writeUTF(msg);//Envio de Mensaje
        DefaultListModel modelo = new DefaultListModel();
        
        for (int i = 0; i < usuarioActivo.size(); i++) {
            modelo.addElement(usuarioActivo.get(i).nombre);
        }
        salidaObjeto =  new ObjectOutputStream(Cliente.getOutputStream());
        salidaObjeto.writeObject(modelo);
    }
    
}
